const data = [
    {
        "category": "",
        "questions": []
    },
    {
        "category": "Ужасы&nbsp;и&nbsp;мистика&nbsp;&#x1f631;",
        "questions": [
            {
                "points": 1,
                "question": "Какой сериал о зомби ни разу не использовал слово \"зомби\"?",
                "answer": "Ходячие мертвецы"
            },
            {
                "points": 2,
                "question": "По произведениям этого автора снято более 60-ти фильмов и 10 сериалов. Как зовут этого автора?",
                "answer": "Стивен Кинг"
            },
            {
                "points": 3,
                "question": "Какому культовому научно-фантастическому фильму с женщиной в главной роли исполнилось 40 лет в прошлом году?",
                "answer": "Чужой"
            },
            {
                "points": 4,
                "question": "Какому сериалу принадлежит этот опенинг?<br><iframe src=\"https://www.youtube.com/embed/-SR47ElKXfs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
                "answer": "Твин пикс"
            },
            {
                "points": 5,
                "question": "Какому нашумевшему сериалу 2018 года принадлежит этот постер?<br><img src='assets/questions/ba0e9d0f561f7420_848x477.jpg'>",
                "answer": "Призраки дома на холме"
            },
            {
                "points": 6,
                "question": "После выхода какого фильма стриминговому сервису Нетфликс пришлось объяснять людям, что ходить по улице с завязанными глазами это опасная затея?",
                "answer": "Птичий короб"
            },
            {
                "points": 7,
                "question": "Из какого фильма этот отрывок?<br><iframe src=\"https://www.youtube.com/embed/XVFDvrV5IQ0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
                "answer": "Крик"
            },
            {
                "points": 8,
                "question": "Из какого сериала этот кадр?<br><img src='assets/questions/Screen-shot-2015-10-08-at-7.15.39-PM.png'>",
                "answer": "Американская история ужасов"
            },
            {
                "points": 9,
                "question": "Стананинский храм США хотел судиться с Нетфликс из-за появления их скульптуры этого демона в сериале \"Леденящие душу приключения Сабрины\".<br><img src='assets/questions/chilling-adventures-of-sabrina-still-Lucien-Greaves.jpg'>",
                "answer": "Бафомет"
            }
        ]
    },
    {
        "category": "Мелодрамы&nbsp;&#x1f491;",
        "questions": [
            {
                "points": 1,
                "question": "Из какого фильма кадр?<br><img src='assets/questions/15-zvezdnyh-par-kotorye-ne-odin-raz-snimalis-vmeste.jpg'>",
                "answer": "Охотники на гангстеров"
            },
            {
                "points": 2,
                "question": "Какой вопрос лежит в названии этого фильма?<br><img src='assets/questions/maxresdefault.jpg'>",
                "answer": "Чего хотят женщины"
            },
            {
                "points": 3,
                "question": "Из какого фильма этот поцелуй?<br><img src='assets/questions/valentines-day-movies-53.jpg?resize=1000%2C600'>",
                "answer": "Грязные танцы"
            },
            {
                "points": 4,
                "question": "Из какого замечательного фильма этот танец?<br><img src='assets/questions/25334794.gif'>",
                "answer": "Любовь и голуби"
            },
            {
                "points": 5,
                "question": "Пожилая женщина рассказывает эротические истории группе водолазов<br><img src='assets/questions/e0a3bc40-0c68-443a-a9a8-e00d773273a6-rose-titanic.png'>",
                "answer": "Титаник"
            },
            {
                "points": 6,
                "question": "У нее мужики - один кобель, второй упырь.",
                "answer": "Сумерки"
            },
            {
                "points": 7,
                "question": "Отгадайте фильм по кадру<br><img src='assets/questions/MCDDEWE_FE083_H.jpeg'>",
                "answer": "Дьявол носит Prada"
            }
        ]
    },
    {
        "category": "Фантастика&nbsp;&#x1f916;",
        "questions": [
            {
                "points": 1,
                "question": "Изначально предполагалось получить $80 миллионов на постановку фильма, однако студия выделила только десять. Получив деньги, режиссеры (тогда еще мужского пола) решили полностью вложить их в первые десять минут фильма. Финальный результат настолько впечатлил студию, что оставшуюся часть денег они получили без всяких проблем. Назовите фильм, изменивший индустрию блокбастеров.",
                "answer": "Матрица"
            },
            {
                "points": 2,
                "question": "Из какого фильма этот кадр?<br><img src='assets/questions/kinopoisk.ru-Interstellar-2514155.jpg'>",
                "answer": "Интерстеллар"
            },
            {
                "points": 3,
                "question": "Угадайте фильм по постеру<br><img src='assets/questions/1314839.jpg'>",
                "answer": "Начало"
            },
            {
                "points": 4,
                "question": "Из какого фильма этот фрагмент?<br><img src='assets/questions/eup9.gif'>",
                "answer": "2001 год: Космическая одиссея"
            },
            {
                "points": 5,
                "question": "Угадайте фильм по фрагменту<br><img src='assets/questions/HFFU.gif'>",
                "answer": "Жить своей жизнью"
            },
            {
                "points": 6,
                "question": "Из какого фильма сцена?<br><video controls><source src='assets/questions/777.webm'></video>",
                "answer": "Назад в будущее"
            },
            {
                "points": 7,
                "question": "Угадайте фильм по кадру<br><img src='assets/questions/kinopoisk.ru-Terminator-2_3A-Judgment-Day-1126879.jpg'>",
                "answer": "Терминатор 2"
            },
            {
                "points": 8,
                "question": "Угадайте фильм по фрагменту звуковой дорожки<br><iframe width=\"115\" height=\"115\" src=\"https://www.youtube.com/embed/AtwZLIBZGlU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen style='width:50px;height:50px'></iframe>",
                "answer": "Звездные Войны"
            },
            {
                "points": 9,
                "question": "В этом легендарном космическом фильме специальная смесь из молока и йогурта была использована в качестве \"крови\" для андроида Бишопа.",
                "answer": "Чужие"
            },
            {
                "points": 10,
                "question": "Из какого фильма этот кадр<br><img src='assets/questions/kinopoisk.ru-Men-in-Black-2287620.jpg'>",
                "answer": "Люди в черном"
            },
            {
                "points": 11,
                "question": "Cо съемок какого фильма этот кадр?<br><img src='assets/questions/kinopoisk.ru-Avatar-1341545.jpg'>",
                "answer": "Аватар"
            },
            {
                "points": 12,
                "question": "Фильм был задуман Стэнли Кубриком в далеком 1969 году. Тридцать лет Кубрик вынашивал эту идею, а к началу создания картины ушел из жизни, но его друг Стивен Спилберг в память о великом режиссере воплотил его намерения в жизнь.",
                "answer": "Искусственный разум"
            },
            {
                "points": 13,
                "question": "Какому фильму принадлежит этот постер?<br><img src='assets/questions/bALe9UG.jpeg'>",
                "answer": "Я - легенда"
            },
            {
                "points": 14,
                "question": "Из какого фильма эти кадры?<br><img src='assets/questions/11.gif'>",
                "answer": "Я, робот"
            },
            {
                "points": 15,
                "question": "Все актеры имеют несколько ролей в фильме, охватывающих разное время, расы и даже пол. Это было достигнуто благодаря пластическому гриму, богатой костюмерной и жесткими временными рамками на переход между ролями для актеров. К примеру, главная актриса рассказала об опыте съемок в образе еврейской женщины 1930-х годов в один день, потом быстрый переход к женщине из древнего племени под руководством режиссеров (в то время уже женского пола).",
                "answer": "Облачный атлас"
            }
        ]
    },
    {
        "category": "Приключения&nbsp;&#x1f6a3;",
        "questions": [
            {
                "points": 1,
                "question": "Оставшись один на один с арабским фехтовальщиком, главный герой этого фильма препочел обойтись одним выстрелом вместо эпичного поединка.",
                "answer": "Индиана Джонс: В поисках утраченного ковчега"
            },
            {
                "points": 2,
                "question": "Основой для какого фильма послужила игра про археолога с двумя серибристыми Heckler & Koch USP Match",
                "answer": "Лара Крофт: Расхитительница гробниц"
            },
            {
                "points": 3,
                "question": "Автор популярных женских романов оказывается в погоне за этим камнем<br><img src='assets/questions/romancing1.jpg'>",
                "answer": "Роман с камнем"
            },
            {
                "points": 4,
                "question": "Что кричит Гендальф в этой сцене?<br><img src='assets/questions/maxresdefault1.jpg'>",
                "answer": "\"Ты не пройдёшь!\"<br>Властелин колец: Братство Кольца"
            },
            {
                "points": 5,
                "question": "Парк, в котором живут клонированные версии вымерших видов.",
                "answer": "Парк юрского периода"
            },
            {
                "points": 6,
                "question": "Начиная с 1933 года, это огромное существо появляось в кино 10 раз.",
                "answer": "Кинг Конг"
            },
            {
                "points": 7,
                "question": "Этот остров стал популярен после выхода фильма про этого персонажа?<br><img src='assets/questions/To9qHuNvP0o.jpg'>",
                "answer": "Джеймс Бонда<br>Фильм: Человек с золотым пистолетом"
            },
            {
                "points": 8,
                "question": "Герой какого фильма спал в лошади и дрался с гризли?",
                "answer": "Выживший"
            },
            {
                "points": 9,
                "question": "У персонажа Мартина Фримана нет времени на допрос злодея. Из какого фильма этот кадр?<br><img src='assets/questions/A8FAmoIQPuQ.jpg'>",
                "answer": "Черная пантера"
            },
            {
                "points": 10,
                "question": "В каком фильме главный герой часто делал этот трюк?<br><iframe src=\"https://www.youtube.com/embed/WCS85-rDOJs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
                "answer": "Доспехи Бога"
            }
        ]
    },
    {
        "category": "Мыльные&nbsp;оперы&nbsp;&#x1f495;",
        "questions": [
            {
                "points": 1,
                "question": "В последней, 2137-ой серии этого сериала, Мейсон всё-таки женился на Джулии, Си-Си вновь сходится с Софией а Келли Кэпвелл отдаёт своё сердце офицеру полиции Коннору МакКейбу.",
                "answer": "Санта-Барбара"
            },
            {
                "points": 2,
                "question": "Все школьницы полюбили этот отечественный сериал не только за актрис, которые играли сами себя из реальной жизни, но и за заводной рок-н-рольный саундтрек.",
                "answer": "Ранетки"
            },
            {
                "points": 3,
                "question": "В этом бразильском сериале, Жади любит Лукаса, у которого есть брат-близнец, который разбивается на вертолёте. К счастью дядя близнецов - талантливый биолог, решает создать клона любимого крестника.",
                "answer": "Клон"
            },
            {
                "points": 4,
                "question": "Главную роль в этом сериале сыграла неподражаемая Наталия Орейро. Оригинальное испанское название сериала - Muneca Brava.",
                "answer": "Дикий Ангел"
            },
            {
                "points": 5,
                "question": "Ультрамодный сериал о похождениях четырёх подруг. Им за 30, они пьют коктейли и ищут в большом городе только одного.",
                "answer": "Секс в большом городе"
            },
            {
                "points": 6,
                "question": "Любовные интриги переплетаются с колдовством трёх сестёр. И да, все любят Фиби.",
                "answer": "Зачарованные"
            }
        ]
    }
];
